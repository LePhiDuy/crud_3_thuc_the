const httpStatus = require('http-status');
const School = require('../models/school.model');
const ApiError = require('../utils/ApiError');

const getSchools = async () => {
  const schools = await School.find({}).populate({
    path: 'classes',
    populate: {
      path: 'students',
    },
  });
  return schools;
};

const createSchool = async (schoolBody) => {
  return School.create(schoolBody);
};

const getSchoolById = async (schoolId) => {
  return School.findById(schoolId);
};

const deleteSchoolById = async (schoolId) => {
  const school = await getSchoolById(schoolId);
  if (!school) {
    throw new ApiError(httpStatus.NOT_FOUND, 'School not found');
  }
  await school.remove();
  return school;
};

const updateBySchoolId = async (schoolId, schoolBody) => {
  const school = await getSchoolById(schoolId);
  if (!school) {
    throw new ApiError(httpStatus.NOT_FOUND, 'School not found');
  }
  // copy properties of schoolBody to school
  Object.assign(school, schoolBody);
  school.save();
  return school;
};

module.exports = {
  getSchools,
  createSchool,
  getSchoolById,
  deleteSchoolById,
  updateBySchoolId,
};
