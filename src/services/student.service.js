const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const Student = require('../models/student.model');

const getStudents = async () => {
  const students = await Student.find({});
  return students;
};

const getStudent = async (studentId) => {
  return Student.findById(studentId).populate('class');
};

const createStudent = async (studentBody) => {
  return Student.create(studentBody);
};

const updateStudent = async (studentId, studentBody) => {
  const student = await getStudent(studentId);
  if (!student) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Student not found');
  }
  Object.assign(student, studentBody);
  student.save();
  return student;
};

const deleteStudent = async (studentId) => {
  const student = await getStudent(studentId);
  if (!student) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Student not found');
  }
  student.remove();
  return student;
};

module.exports = {
  getStudents,
  getStudent,
  createStudent,
  updateStudent,
  deleteStudent,
};
