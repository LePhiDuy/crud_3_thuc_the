const httpStatus = require('http-status');
const Class = require('../models/class.model');
const ApiError = require('../utils/ApiError');

const getClasses = async () => {
  const students = await Class.find({}).populate('school');
  return students;
};
const getClassById = async (classId) => {
  return Class.findById(classId).populate('school');
};

const createClass = async (classBody) => {
  return Class.create(classBody);
};

const updateClassById = async (classId, classBody) => {
  const classFound = await getClassById(classId);
  if (!classFound) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Class not found');
  }
  Object.assign(classFound, classBody);
  classFound.save();
  return classFound;
};

const deleteClassById = async (classId) => {
  const classFound = await getClassById(classId);
  if (!classFound) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Class not found');
  }
  await classFound.remove();
  return classFound;
};

module.exports = {
  getClasses,
  getClassById,
  createClass,
  updateClassById,
  deleteClassById,
};
