const httpStatus = require('http-status');
// const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { studentService } = require('../services');

const getStudents = catchAsync(async (req, res) => {
  const students = await studentService.getStudents();
  res.send(students);
});

const getStudent = catchAsync(async (req, res) => {
  const student = await studentService.getStudent(req.params.id);
  if (!student) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Student not found');
  }
  res.send(student);
});

const createStudent = catchAsync(async (req, res) => {
  const student = await studentService.createStudent(req.body);
  res.send(student);
});

const updateStudent = catchAsync(async (req, res) => {
  const student = await studentService.updateStudent(req.params.id, req.body);
  res.send(student);
});

const deleteStudent = catchAsync(async (req, res) => {
  await studentService.deleteStudent(req.params.id);
  res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
  getStudents,
  getStudent,
  createStudent,
  updateStudent,
  deleteStudent,
};
