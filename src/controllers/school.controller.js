const httpStatus = require('http-status');
// const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { schoolService } = require('../services');

const getSchools = catchAsync(async (req, res) => {
  const schools = await schoolService.getSchools();
  res.send(schools);
});

const getSchool = catchAsync(async (req, res) => {
  const school = await schoolService.getSchoolById(req.params.id);
  if (!school) {
    throw new ApiError(httpStatus.NOT_FOUND, 'School not found');
  }
  res.send(school);
});

const createSchool = catchAsync(async (req, res) => {
  const school = await schoolService.createSchool(req.body);
  res.send(school);
});

const deleteSchool = catchAsync(async (req, res) => {
  await schoolService.deleteSchoolById(req.params.id);
  res.status(httpStatus.NO_CONTENT).send();
});

const updateSchool = catchAsync(async (req, res) => {
  const school = await schoolService.updateBySchoolId(req.params.id, req.body);
  return res.send(school);
});

module.exports = {
  getSchools,
  createSchool,
  deleteSchool,
  updateSchool,
  getSchool,
};
