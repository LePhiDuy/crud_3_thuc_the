const httpStatus = require('http-status');
// const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { classService } = require('../services');

const getClasses = catchAsync(async (req, res) => {
  const classes = await classService.getClasses();
  res.send(classes);
});

const getClass = catchAsync(async (req, res) => {
  const classFind = await classService.getClassById(req.params.id);
  if (!classFind) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Class not found');
  }
  return res.send(classFind);
});

const createClass = catchAsync(async (req, res) => {
  const classCreated = await classService.createClass(req.body);
  return res.send(classCreated);
});

const updateClass = catchAsync(async (req, res) => {
  const classUpdated = await classService.updateClassById(req.params.id, req.body);
  return res.send(classUpdated);
});

const deleteClass = catchAsync(async (req, res) => {
  await classService.deleteClassById(req.params.id);
  res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
  getClasses,
  getClass,
  createClass,
  updateClass,
  deleteClass,
};
