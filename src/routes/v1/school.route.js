const express = require('express');
const schoolController = require('../../controllers/school.controller');

const router = express.Router();

router.get('/', schoolController.getSchools);
router.get('/:id', schoolController.getSchool);
router.post('/', schoolController.createSchool);
router.delete('/:id', schoolController.deleteSchool);
router.put('/:id', schoolController.updateSchool);

module.exports = router;
