const express = require('express');
const classController = require('../../controllers/class.controller');

const router = express.Router();

router.get('/', classController.getClasses);
router.get('/:id', classController.getClass);
router.post('/', classController.createClass);
router.put('/:id', classController.updateClass);
router.delete('/:id', classController.deleteClass);

module.exports = router;
