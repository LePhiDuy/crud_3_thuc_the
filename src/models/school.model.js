const mongoose = require('mongoose');

const { Schema } = mongoose;

const SchoolSchemal = new Schema(
  {
    name: { type: String, required: true },
  },
  { timestamps: true }
);

const School = mongoose.model('School', SchoolSchemal);
module.exports = School;
