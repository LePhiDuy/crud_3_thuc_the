const mongoose = require('mongoose');

const { Schema } = mongoose;

const StudentSchemal = new Schema(
  {
    name: { type: String, required: true },
    age: { type: Number, required: true },
    gender: { type: Boolean },
    class: { type: Schema.Types.ObjectId, ref: 'Class' },
  },
  {
    timestamps: true,
  }
);

const Student = mongoose.model('Student', StudentSchemal);

module.exports = Student;
