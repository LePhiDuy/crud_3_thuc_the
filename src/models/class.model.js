const mongoose = require('mongoose');

// eslint-disable-next-line prefer-destructuring
const { Schema } = mongoose;

// eslint-disable-next-line no-unused-vars
const ClassSchema = new Schema(
  {
    name: { type: String, required: true },
    school: { type: Schema.Types.ObjectId, ref: 'School' },
  },
  {
    timestamps: true,
  }
);
const Class = mongoose.model('Class', ClassSchema);

module.exports = Class;
